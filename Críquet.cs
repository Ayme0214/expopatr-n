﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpoPatrón1
{
    //la clase Críquet extiende la clase Juego e implementa cada uno de los métodos abstractos
    public class Críquet: Juego //aplicamos herencia desde la superclase Juego
    {
        public override void finalizarJuego() //sobreescribimos el método finalizarJuego() con la palabra reservada override
        {
            Console.WriteLine("Juego de Críquet terminado!"); //mostramos mensaje
        }
        public override void inicializar() //sobreescribimos el método inicializar() con la palabra reservada override
        {
            Console.WriteLine("Juego de Críquet inicializado! Empieza a jugar"); //mostramos mensaje
        }
        public override void empezarJuego() //sobreescribimos el método empezarJuego() con la palabra reservada override
        {
            Console.WriteLine("Juego de Críquet comenzado. Disfruta del juego"); //mostramos mensaje
        }
    }
}

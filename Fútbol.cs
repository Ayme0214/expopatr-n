﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpoPatrón1
{
    //la clase Fútbol extiende la clase Juego e implementa cada uno de los métodos abstractos
    public class Fútbol: Juego //aplicamos herencia desde la superclase Juego
    {
        public override void finalizarJuego() //sobreescribimos el método finalizarJuego() con la palabra reservada override
        {
            Console.WriteLine("Juego de Fútbol terminado!"); //mostramos mensaje
        }
        public override void inicializar() //sobreescribimos el método inicializar() con la palabra reservada override
        {
            Console.WriteLine("Juego de Fútbol inicializado! Empieza a jugar"); //mostramos mensaje
        }
        public override void empezarJuego() //sobreescribimos el método empezarJuego() con la palabra reservada override
        {
            Console.WriteLine("Juego de Fútbol comenzado. Disfruta del juego"); //mostramos mensaje
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using System.Text;

namespace ExpoPatrón1
{
    public abstract class Juego //definimos la clase Juego como abstracta
    {
        public void TemplateMethod() //definimos el método plantilla
        {
            // ubicamos dentro del metodo plantilla los métodos abstractos que creamos
            inicializar();
            empezarJuego();
            finalizarJuego();
        }

        public abstract void inicializar(); //método abstracto para inicializar
        public abstract void empezarJuego(); //método abstracto para empezar el juego
        public abstract void finalizarJuego(); //método abstracto para finalizar juego
    }
}

﻿using System;

namespace ExpoPatrón1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Programa que demuestra la manera definida de jugar Críquet y Fútbol"); //mostramos el mensaje inicial del programa
            Console.WriteLine();

            Console.WriteLine("Críquet"); //mostramos mensaje
            Juego juego = new Críquet(); //creamos un objeto "juego"
            juego.TemplateMethod(); //con el objeto "juego" llamamos al TemplateMethod en instancias de la clase Críquet

            Console.WriteLine();
            Console.WriteLine("Fútbol"); //mostramos mensaje
            Juego juego1 = new Fútbol(); //creamos un objeto "juego1"
            juego1.TemplateMethod(); //con el objeto "juego1" llamamos al TemplateMethod en instancias de la clase Fútbol

            Console.Read();
        }
    }
}

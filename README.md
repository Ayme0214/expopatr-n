Este código muestra un ejemplo del patrón de diseño de 
comportamiento Plantilla Método o "Método Plantilla"

El ejercicio se basa en crear una clase abstracta de juego 
la cual define operaciones con un método de plantilla 
establecida como final para que no pueda ser anulado

Las clases Críquet y Fútbol son clases concretas que 
extienden el juego y anulan sus métodos